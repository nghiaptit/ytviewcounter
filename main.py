import logging
import logging.config
import os

from count_biz import CountBiz


def config_log():
    log_filename = "logs/count.log"
    os.makedirs(os.path.dirname(log_filename), exist_ok=True)

    logging.config.fileConfig('logging.conf')


if __name__ == '__main__':
    config_log()

    biz = CountBiz()
    biz.count()



