import openpyxl
import pandas as pd
from openpyxl import load_workbook
import os

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


class Excel:

    def read_file_excel(self, path_file):
        df = pd.read_excel(path_file)
        lst_code = df['ID_PLAYLIST'].to_list()

        result = []
        for item in lst_code:
            xx = str(item).strip()
            result.append(xx)
        return result

    def write(self, list_view, filename):
        logger.info("Write result to excel file !")
        workbook = openpyxl.load_workbook(filename)
        sheet = workbook.get_sheet_by_name('Sheet1')

        r = sheet.max_row
        for i in range(2,r+1):
            id_playlist = sheet.cell(i,1).value

            view = self.get_view_from_id_playlist(list_view,id_playlist)
            if view is not None:
                sheet.cell(i,2).value = view.title
                sheet.cell(i,3).value = view.view_content
            else:
                sheet.cell(i,4).value = "Error"

        workbook.save(filename)
        workbook.close()

        logger.info("Write file complete !")

    def get_view_from_id_playlist(self,list_view,id_playlist):
        for view in list_view:
            if view.id_playlist == id_playlist:
                return view

        return None
