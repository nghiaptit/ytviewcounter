import logging
import urllib.request

import jsons as jsons
import requests
from bs4 import BeautifulSoup as BS

from excel import Excel
from models.view import View

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


class CountBiz:
    def __init__(self):
        logger.info("Init count biz")

    def count(self):
        path_file = "Danh sach playlist.xlsx"

        excel = Excel()
        list_id_playlist = excel.read_file_excel(path_file)

        list_item = []

        for id_playlist in list_id_playlist:
            view = self.get_view_from_idplaylist(id_playlist)
            if view is not None:
                list_item.append(view)

        excel.write(list_item, path_file)

    def get_view_from_idplaylist(self, id_playlist):
        url_pattern = 'https://www.youtube.com/playlist?list='
        url = url_pattern + id_playlist

        r = requests.get(url)
        html = r.text
        soup = BS(html, "html.parser")
        lst_script = soup.find_all("script")

        view = View()

        for script in lst_script:
            list_content = script.contents
            if len(list_content) == 0:
                continue

            content = list_content[0]

            index = str(content).find('ytInitialData = ')
            if index >= 0:
                views = self.get_content_views(str(content))
                title = self.get_title_playlist(str(content))

                view.view_content = views
                view.title = title
                view.id_playlist = id_playlist
                return view

        return None

    def get_content_views(self, content):
        json_str = str(content).split('ytInitialData = ')[1].split(';')[0]
        json = jsons.loads(json_str)
        view = json['sidebar']['playlistSidebarRenderer']['items'][0]['playlistSidebarPrimaryInfoRenderer']['stats'][1][
            'simpleText']
        return view

    def get_title_playlist(self, content):
        json_str = str(content).split('ytInitialData = ')[1].split(';')[0]
        json = jsons.loads(json_str)
        title = json['metadata']['playlistMetadataRenderer']['title']
        return title
